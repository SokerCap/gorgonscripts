<?php

/** MediaWiki API class
*	Uses curl to login and do stuff and things
*
**/

class MediaWiki {
	
	private $cookie_jar;

	private $ch;
	private $username;
	private $pwd;
	private $user_agent = 'ProjectGorgon Wiki Updater by Rekos/Sokercap';
	private $wiki_url = 'http://projectgorgon.com/w/api.php';
	private $login_url = 'http://projectgorgon.com/component/users/?task=user.login';

	public function __construct($username, $pwd) {
		$this->cookie_jar  = tempnam('/tmp','cookie');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_COOKIEJAR, $this->cookie_jar);
        //curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLINFO_HEADER_OUT,true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch,CURLOPT_HEADER, true); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, 'http://projectgorgon.com/index.php?option=com_users&view=login&return=L3c=');
        //curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_REFERER,'http://projectgorgon.com/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        	'Origin: http://projectgorgon.com',
        	'Content-Type:application/x-www-form-urlencoded'
        	));
        $output = curl_exec($ch);
        curl_close($ch);
        //parse index for form datas
        $matches = null; $return_id=null; $form_id=null;
        if( preg_match('|return"\svalue="(.+?)"|',$output,$matches) ){
        	$return_id=$matches[1];
        }
        if( preg_match('|hidden"\sname="(.+?)" value="1"|',$output,$matches) ){
        	$form_id=$matches[1];
        }

		
		$this->username = $username;
		$this->pwd = $pwd;
		$post_data = "username=".$this->username."&password=".$this->pwd.'';
		$post_d = array(
			'username'=>$this->username,
			'password'=>$this->pwd
			);
		if($form_id) {
			$post_data .= "&".$form_id."=1";
			$post_d[$form_id] = "1";
		}
		if(0 &&$return_id){
			$post_data .= "&return=".$return_id;
			$post_d['return'] = $return_id;
		}
		$post_d['return'] = "L3c=";
		print $post_data."\n";
		//curl_setopt($ch,CURLOPT_COOKIEJAR, $this->cookie_jar);
        //curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLINFO_HEADER_OUT,true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_HEADER, true); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, 'http://projectgorgon.com/component/users/?task=user.login');
		curl_setopt($ch, CURLOPT_POST, sizeof($post_d) );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_d);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_REFERER,'http://projectgorgon.com/index.php?option=com_users&view=login&return=L3c=');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        	'Origin: http://projectgorgon.com',
        	'Host: projectgorgon.com',
        	'Referer: http://projectgorgon.com/index.php?option=com_users&view=login&return=L3c=',
        	'Content-Type: application/x-www-form-urlencoded'
        	));
        //curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //curl_setopt($ch,CURLOPT_HEADER, true); 
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $curl_info = curl_getinfo($ch, CURLINFO_HEADER_OUT);
        print "CURL INFO: \n".print_r($curl_info,true)."\n";
        $output=curl_exec($ch);
        print "Finished login at ".curl_getinfo($ch, CURLINFO_EFFECTIVE_URL)."\n";
    	curl_close($ch);

    	print "Got outpuut!\n".strlen($output)."\n";
    	file_put_contents("login_dump.html",$output);
    	if(strpos($output,'Location')!==false){
    		print "Found location header: ".substr($output, 0, strpos($output,"Content-Type"))."\n";
    	}
    	if(strpos($output,'Sokercap')!==false){
    		print "Logged in!\n";
    	} else {
    		die("Not logged in!\n");
    	}

  		return true;
	}


	public function fetchUser($usr) {
		$ch = $this->getCurl();
		$query = '?action=query&format=json&prop=revisions&rvprop=content&titles=User:'.$usr;
		curl_setopt($ch, CURLOPT_URL, $this->wiki_url.$query);
		return $this->execCurlJSON($ch);
	}

	public function appendPage($page,$content){

	}

	public function editPage($page,$content) {
		//print "Editing $page .. \n$content\n";
		$ch = $this->getCurl();
		$query='?action=query&format=json&prop=info|revisions&intoken=edit&rvprop=timestamp&titles='.$page;
		curl_setopt($ch, CURLOPT_URL, $this->wiki_url.$query);
		$res = $this->execCurlJSON($ch);
		print "Edit Token!\n".print_r($res)."\n";	
	}

	public function getWatchlist(){
		$query = '?action=query&format=json&list=watchlist&wlprop=ids|title|timestamp|user|comment';
		$ch = $this->getCurl();
		curl_setopt($ch, CURLOPT_URL, $this->wiki_url.$query);
		$res = $this->execCurlJSON($ch);
		return $res;
	}


	private function execCurlJSON($ch){
		$output = curl_exec($ch);
		$jsonStart = strpos($output,'{"');
		if($jsonStart!==false){
    		$output = substr($output, $jsonStart);
    	} else {
    		echo "Could not find start of json content\n";
    		return $output;
    	}
    	$res = json_decode($output);
    	return $res;
	}

	private function getCurl(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_URL, $this->wiki_url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //curl_setopt($ch,CURLOPT_HEADER, false); 
        curl_setopt($ch,CURLOPT_COOKIEJAR, $this->cookie_jar);
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);
        return $ch;
	}
	private function setPOST($ch,$post_data){
		curl_setopt($ch, CURLOPT_POST, count($post_data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	}
}

?>