<?php

//Fetch ProjectGorgon data files and import into mongo

$url = 'http://cdn.projectgorgon.com/v';
$version = '233';
$m = new MongoClient();
$db = $m->gorgon;


print "Fetching ".$url."\n";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url.$version."/data/");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);
curl_close($ch);

print "Fetched .. ".substr($output, 0,20)."\n";
preg_match_all('|href="(\w+).json|U', $output, $matches);


if(!$matches){
	die("Could not find any json links!\n");
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
for($i=0;$i<count($matches[1]);$i++) {
	print "Downloading ".$matches[1][$i].".json .. ";
	curl_setopt($ch, CURLOPT_URL, $url.$version."/data/".$matches[1][$i].".json");
	$json = curl_exec($ch);
	file_put_contents("downloaded/".$matches[1][$i].".json", $json);
	print "Saved File.. \n";
	//insert into mongo here
	$col = $db->{$matches[1][$i]};
	print $col->count()." existing records found ..\n";
	//$col->remove();
	$inserted = 0;
	$updated = 0;
	$records = json_decode( utf8_encode($json));
	foreach($records as $full_title => $record) {
		$record->full_id = $full_title;
		if(preg_match('|^(\w+)_(\w+)$|U',$full_title,$title_matches) ) {
			$record->id = $title_matches[2];
			$record->id_2 = $title_matches[1];
		}
		$existing = $col->findOne(array('full_id'=>$full_title));
		$recordA = (array)$record;
		if($existing){
			$new = $recordA + $existing;
			$new['version'] = $version;
			$new['dt_modify'] = new MongoDate();
			$col->update(array('full_id'=>$full_title), $new);
			$updated++;
		} else {
			$recordA['dt_modify'] = new MongoDate();
			$recordA['dt_create'] = new MongoDate();
			$recordA['version'] = $version;
			$col->insert($recordA);
			$inserted++;
		}
		//$col->insert($record);
	}
	print "\nDone! Inserted ".$inserted." records! Updated: ".$updated." records!\n";

	print "\n";
}
curl_close($ch);

