<?php

//Update recipe item ids with the item name and value for easier browsing

$m = new MongoClient();
$db = $m->gorgon;

$col = $db->recipes;
$cursor = $col->find();

$colItems = $db->items;
$items = array();


$fields = array('Ingredients','ResultItems');

print "Looping through ".$col->count()." recipes.. \n";
$recipes_updated = 0;
$items_updated = 0;
foreach($cursor as $doc){
	foreach($fields as $fieldname) {
		if( isset($doc[$fieldname]) ) {
			$total_value = 0;
			for($i=0;$i<count($doc[$fieldname]);$i++){



				if(!isset($doc[$fieldname][$i]['ItemCode'])) continue;
				$doc[$fieldname][$i]['ItemCode'] = $doc[$fieldname][$i]['ItemCode']."";
				if( isset( $items[ $doc[$fieldname][$i]['ItemCode'] ] ) ) {
					$doc[$fieldname][$i]['ItemName'] = $items[$doc[$fieldname][$i]['ItemCode']]['Name'];
					$doc[$fieldname][$i]['ItemValue'] = $items[$doc[$fieldname][$i]['ItemCode']]['Value'];
					$doc[$fieldname][$i]['ItemMongoID'] = $items[$doc[$fieldname][$i]['ItemCode']]['_id']."";
					$total_value += $doc[$fieldname][$i]['ItemValue'] * $doc[$fieldname][$i]['StackSize'];
					$items_updated++;
				} else {
					$results = $colItems->findOne(array('id'=> $doc[$fieldname][$i]['ItemCode'] ));
					if($results) {
						$items[$doc[$fieldname][$i]['ItemCode']] = $results;
						//print "got results.. \n".print_r($results,true)."\n"; exit;
						$doc[$fieldname][$i]['ItemName'] = $items[$doc[$fieldname][$i]['ItemCode']]['Name'];
						$doc[$fieldname][$i]['ItemValue'] = $items[$doc[$fieldname][$i]['ItemCode']]['Value'];
						$doc[$fieldname][$i]['ItemMongoID'] = $items[$doc[$fieldname][$i]['ItemCode']]['_id']."";
						$total_value += $doc[$fieldname][$i]['ItemValue'] * $doc[$fieldname][$i]['StackSize'];
						$items_updated++;
					} else {
						print "Could not find item ".$doc[$fieldname][$i]['ItemCode']." .. \n"; 
					}
				}
			}
			$doc[$fieldname.'_value'] = $total_value;

		}
	}
	if(isset($doc['Ingredients_value']) && isset($doc['ResultItems_value']) ) {
		$doc['ValueRatio'] = ($doc['Ingredients_value']!=0) ? (floatval($doc['ResultItems_value'])/floatval($doc['Ingredients_value']) ) : 0;
	}

	//check for survey recipes
	if(isset($doc['ResultEffects']) ) {
		$fieldname = 'ResultEffects';
		foreach($doc['ResultEffects'] as $i=>$v) {
			if( strpos($doc[$fieldname][$i], "CreateMiningSurvey") !== FALSE || strpos($doc[$fieldname][$i], "CreateGeologySurvey") !== FALSE ){
				$matches = null;
				preg_match('/Create.+Survey.+?\((.+?)\)/', $doc[$fieldname][$i], $matches);
				if($matches ){
					$results = $colItems->findOne(array('InternalName'=> $matches[1]));
					$doc['ResultSurvey'] = $results;
				}
			}
		}
	}

	$col->update(array('_id'=>$doc['_id']), $doc); //should upsert it?
	$recipes_updated++;
}

print "DONE!\n";
print $recipes_updated." recipes updated\n";
print $items_updated." Items updated. ".count($items)." total items\n";