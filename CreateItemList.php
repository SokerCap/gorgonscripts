<?php

//Get all equipment with unique effects and create a table of them

$m = new MongoClient();
$db = $m->gorgon;

$col = $db->items;
$query = array(
	"Keywords"=>"Equipment",
	"ServerInfo.Effects"=>array('$ne'=>null),
);
$fields = array('Name'=>true,'EquipSlot'=>true,'Description'=>true,'Keywords'=>true,'ServerInfo.Effects'=>true);
$cursor = $col->find($query, $fields)->sort(array("EquipSlot"=>1,"Name"=>1));

print "Looping through ".$col->count()." items.. \n";
$items_found = 0;
$currentSlot = null;

$fh = fopen("wikify/ListOfEquipment.txt","w");

//list of keywords to check against and include under item attributes
$keywords = array(
	'ClothArmor',
	'Clothing',
	'MetalArmor',
	'LeatherArmor',
	'Fashionable',
	'NecromancyGem',
	'FireStaff',
	'Sword',
	'Wooden',
	'ChemistryEquipment',
	'Bow'
	);


foreach($cursor as $doc){
	if(count($doc['ServerInfo']['Effects'])<1) continue;
	if(preg_match('/Temp armor/',$doc['Name']) ) continue;
	if($currentSlot != $doc['EquipSlot']) {
		if($currentSlot){
			fwrite($fh, "|}\n<br>\n");
		}
		fwrite($fh, "{| class='table mw-collapsible mw-collapsed' style='width:100%;'\n
|+<div align='left'><h3>".$doc['EquipSlot']."</h3></div>\n
! Name !! Description !! Attributes !! Effects\n");
		$currentSlot = $doc['EquipSlot'];
	}
	$line = "|-\n| ".$doc['Name']." || ".$doc['Description']." || ";
	//TODO:: replace internal names for things like armor, power whatever with human readable
	$attributes = array_intersect($keywords, $doc['Keywords']);
	foreach($attributes as $attr) {
		$line .= "\n:".$attr;
	}
	if(count($attributes)>0) $line .= "\n";

	$line .= " || ";
	foreach($doc['ServerInfo']['Effects'] as $effect){
		$matches = false;
		if(preg_match('/^Armor\((\d+?)\)/',$effect, $matches) ){
			$effect = "Armor: ".$matches[1];
		} elseif( preg_match('/^EquipmentBoost\((.+?),\s*(.+?)\)/',$effect, $matches) ) {
			$effect = $matches[1].": ".$matches[2];
		} elseif( preg_match('/^SetTSysGuidance\((.+?),(.+?)\)/',$effect, $matches) ) {
			$effect = "Set loot preference to ".$matches[1]." and ".$matches[2];
		} elseif( preg_match('/^EquipmentAbilityBoost\((.+?),\s*(.+?)\)/',$effect, $matches) ) {
			$effect = $matches[1].": +".$matches[2]." damage";
		}
		$line .= "\n:".$effect;
	}
	$line .= "\n";
	fwrite($fh, $line);
}

fclose($fh);