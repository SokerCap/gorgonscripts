{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Name !! Description !! Special !! style="width: 11%; text-align: center;" | Damage !! style="width: 11%; text-align: center;" | Stats !! Gear Required !! style="width: 5%; text-align: center;" |
|-
| 0 || Frenzy 3 || Bite yourself to work up a frenzy. || You take 45 Trauma damage over 15 seconds. You deal +30% Crushing damage for 10 seconds. ||  || <b>Power Cost:</b> 6<br><b>Range:</b> 5<br><b>Cooldown:</b> 10 || form:PotbellyPig, 
|-
| 1 || Pig Bite || Bite your enemy. ||  || <b>Damage: </b>7 Crushing || <b>Power Cost:</b> 4<br><b>Range:</b> 5<br><b>Cooldown:</b> 1 || form:PotbellyPig, 
|-
| 3 || Pig Rend || Tear into your enemy, causing it to bleed. || Deals 15 Trauma damage over 15 seconds. || <b>Damage: </b>13 Crushing || <b>Power Cost:</b> 20<br><b>Range:</b> 5<br><b>Cooldown:</b> 5 || form:PotbellyPig, 
|-
| 5 || Truffle Sniff || Search for a hidden mushroom in the area. ||  ||  || <b>Power Cost:</b> 5<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 6 || Strategic Chomp || Chomp an enemy in their most vulnerable spot. ||  || <b>Damage: </b>10 Crushing || <b>Power Cost:</b> 9<br><b>Range:</b> 5<br><b>Cooldown:</b> 7 || form:PotbellyPig, 
|-
| 7 || Mudbath || Take advantage of your caked-on mud to protect you from harm. || You take 20% less damage from all attacks for 8 seconds. ||  || <b>Power Cost:</b> 7<br><b>Range:</b> 5<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 8 || Sustaining Snack || Consume a Parasol Mushroom to regain 25 health. ||  ||  || <b>Power Cost:</b> 8<br><b>Range:</b> 4<br><b>Cooldown:</b> 15 || form:PotbellyPig, 
|-
| 10 || Pig Bite 2 || Bite your enemy. ||  || <b>Damage: </b>10 Crushing || <b>Power Cost:</b> 5<br><b>Range:</b> 5<br><b>Cooldown:</b> 1 || form:PotbellyPig, 
|-
| 11 || Frenzy || Bite yourself to work up a frenzy. || You take 15 Trauma damage over 15 seconds. You deal +10% Crushing damage for 10 seconds. ||  || <b>Power Cost:</b> 8<br><b>Range:</b> 5<br><b>Cooldown:</b> 10 || form:PotbellyPig, 
|-
| 12 || Pig Punt || Kick an enemy away from you. || Flings opponent backwards || <b>Damage: </b>10 Crushing || <b>Power Cost:</b> 8<br><b>Range:</b> 5<br><b>Cooldown:</b> 7 || form:PotbellyPig, 
|-
| 13 || Terror Dash || Increase your run speed dramatically for 5 seconds. || You gain 9 sprint speed for 5 seconds. ||  || <b>Power Cost:</b> 8<br><b>Range:</b> 4<br><b>Cooldown:</b> 60 || form:PotbellyPig, 
|-
| 15 || Truffle Sniff 2 || Search for a hidden mushroom in the area. ||  ||  || <b>Power Cost:</b> 15<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 18 || Sustaining Snack 2 || Consume a Mycena Mushroom to regain 50 health. ||  ||  || <b>Power Cost:</b> 9<br><b>Range:</b> 4<br><b>Cooldown:</b> 15 || form:PotbellyPig, 
|-
| 19 || Strategic Chomp 2 || Chomp an enemy in their most vulnerable spot. ||  || <b>Damage: </b>21 Crushing || <b>Power Cost:</b> 14<br><b>Range:</b> 5<br><b>Cooldown:</b> 7 || form:PotbellyPig, 
|-
| 20 || Pig Bite 3 || Bite your enemy. ||  || <b>Damage: </b>15 Crushing || <b>Power Cost:</b> 6<br><b>Range:</b> 5<br><b>Cooldown:</b> 1 || form:PotbellyPig, 
|-
| 22 || Squeal || Scream horrendously, potentially spooking nearby foes. (Chances of causing fear are based on your Pig level and the targets' mental defenses.) || Targets flee for 5 seconds, or until attacked. || <b>Damage: </b>15 Nature || <b>Power Cost:</b> 10<br><b>Range:</b> 20<br><b>AoE:</b> 20<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 24 || Pig Rend 2 || Tear into your enemy, causing it to bleed. || Deals 30 Trauma damage over 15 seconds. || <b>Damage: </b>33 Crushing || <b>Power Cost:</b> 33<br><b>Range:</b> 5<br><b>Cooldown:</b> 5 || form:PotbellyPig, 
|-
| 25 || Truffle Sniff 3 || Search for a hidden mushroom in the area. ||  ||  || <b>Power Cost:</b> 25<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 27 || Mudbath 2 || Take advantage of your caked-on mud to protect you from harm. || You take 30% less damage from all attacks for 8 seconds. ||  || <b>Power Cost:</b> 9<br><b>Range:</b> 5<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 28 || Sustaining Snack 3 || Consume a Boletus Mushroom to regain 75 health. ||  ||  || <b>Power Cost:</b> 10<br><b>Range:</b> 4<br><b>Cooldown:</b> 15 || form:PotbellyPig, 
|-
| 30 || Pig Bite 4 || Bite your enemy. ||  || <b>Damage: </b>20 Crushing || <b>Power Cost:</b> 6<br><b>Range:</b> 5<br><b>Cooldown:</b> 1 || form:PotbellyPig, 
|-
| 32 || Pig Punt 2 || Kick an enemy away from you. || Flings opponent backwards || <b>Damage: </b>24 Crushing || <b>Power Cost:</b> 11<br><b>Range:</b> 5<br><b>Cooldown:</b> 7 || form:PotbellyPig, 
|-
| 34 || Frenzy 2 || Bite yourself to work up a frenzy. || You take 30 Trauma damage over 15 seconds. You deal +20% Crushing damage for 10 seconds. ||  || <b>Power Cost:</b> 11<br><b>Range:</b> 5<br><b>Cooldown:</b> 10 || form:PotbellyPig, 
|-
| 35 || Harmlessness || Feign harmlessness, convincing your enemy that they could be spending their time more productively than by killing you. || Target decides not to attack you for 5-10 seconds (even if you attack it during that time). ||  || <b>Power Cost:</b> 11<br><b>Range:</b> 40<br><b>Cooldown:</b> 60 || form:PotbellyPig, 
|-
| 35 || Truffle Sniff 4 || Search for a hidden mushroom in the area. ||  ||  || <b>Power Cost:</b> 35<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 37 || Terror Dash 2 || Increase your run speed dramatically for 10 seconds. || You gain 9 sprint speed for 10 seconds. ||  || <b>Power Cost:</b> 12<br><b>Range:</b> 4<br><b>Cooldown:</b> 60 || form:PotbellyPig, 
|-
| 38 || Sustaining Snack 4 || Consume an Agaricus Mushroom to regain 100 health. ||  ||  || <b>Power Cost:</b> 12<br><b>Range:</b> 4<br><b>Cooldown:</b> 15 || form:PotbellyPig, 
|-
| 39 || Squeal 2 || Scream horrendously, potentially spooking nearby foes. (Chances of causing fear are based on your Pig level and the targets' mental defenses.) || Targets flee for 8 seconds, or until attacked. || <b>Damage: </b>32 Nature || <b>Power Cost:</b> 12<br><b>Range:</b> 20<br><b>AoE:</b> 20<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 40 || Pig Bite 5 || Bite your enemy. ||  || <b>Damage: </b>30 Crushing || <b>Power Cost:</b> 7<br><b>Range:</b> 5<br><b>Cooldown:</b> 1 || form:PotbellyPig, 
|-
| 43 || Mudbath 3 || Take advantage of your caked-on mud to protect you from harm. || You take 40% less damage from all attacks for 8 seconds. ||  || <b>Power Cost:</b> 11<br><b>Range:</b> 5<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 45 || Truffle Sniff 5 || Search for a hidden mushroom in the area. ||  ||  || <b>Power Cost:</b> 45<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 46 || Strategic Chomp 3 || Chomp an enemy in their most vulnerable spot. ||  || <b>Damage: </b>61 Crushing || <b>Power Cost:</b> 23<br><b>Range:</b> 5<br><b>Cooldown:</b> 7 || form:PotbellyPig, 
|-
| 47 || Pig Punt 3 || Kick an enemy away from you. || Flings opponent backwards || <b>Damage: </b>48 Crushing || <b>Power Cost:</b> 13<br><b>Range:</b> 5<br><b>Cooldown:</b> 7 || form:PotbellyPig, 
|-
| 48 || Sustaining Snack 5 || Consume a Blusher Mushroom to regain 125 health. ||  ||  || <b>Power Cost:</b> 13<br><b>Range:</b> 4<br><b>Cooldown:</b> 15 || form:PotbellyPig, 
|-
| 50 || Pig Bite 6 || Bite your enemy. ||  || <b>Damage: </b>48 Crushing || <b>Power Cost:</b> 8<br><b>Range:</b> 5<br><b>Cooldown:</b> 1 || form:PotbellyPig, 
|-
| 51 || Squeal 3 || Scream horrendously, potentially spooking nearby foes. (Chances of causing fear are based on your Pig level and the targets' mental defenses.) || Targets flee for 10 seconds, or until attacked. || <b>Damage: </b>56 Nature || <b>Power Cost:</b> 14<br><b>Range:</b> 20<br><b>AoE:</b> 20<br><b>Cooldown:</b> 30 || form:PotbellyPig, 
|-
| 54 || Pig Rend 3 || Tear into your enemy, causing it to bleed. || Deals 45 Trauma damage over 15 seconds. || <b>Damage: </b>119 Crushing || <b>Power Cost:</b> 46<br><b>Range:</b> 5<br><b>Cooldown:</b> 5 || form:PotbellyPig, 
|}
<!-- This is the end of the auto generated content -->
