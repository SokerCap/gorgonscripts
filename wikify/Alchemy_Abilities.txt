{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Name !! Description !! Special !! style="width: 11%; text-align: center;" | Damage !! style="width: 11%; text-align: center;" | Stats !! Gear Required !! style="width: 5%; text-align: center;" |
|-
| 10 || Poison Blade || Jab a Weak Poisoned Knife into your target, dealing instant Poison damage. ||  || <b>Damage: </b>25 Poison || <b>Power Cost:</b> 27<br><b>Range:</b> 5<br><b>Cooldown:</b> 6 || - 
|-
| 20 || Poison Blade 2 || Jab a Modest Poisoned Knife into your target, dealing instant Poison damage. ||  || <b>Damage: </b>50 Poison || <b>Power Cost:</b> 31<br><b>Range:</b> 5<br><b>Cooldown:</b> 6 || - 
|-
| 30 || Poison Blade 3 || Jab a Dangerous Poisoned Knife into your target, dealing instant Poison damage. ||  || <b>Damage: </b>80 Poison || <b>Power Cost:</b> 36<br><b>Range:</b> 5<br><b>Cooldown:</b> 6 || - 
|-
| 30 || Wolfsbane Blade || Jab a Modest Wolfsbaned Knife into your target, dealing instant Poison damage, and additional damage to wolves and lycanthropes. || Deals +80 damage over 10 seconds to wolfsbane-vulnerable targets || <b>Damage: </b>60 Poison || <b>Power Cost:</b> 36<br><b>Range:</b> 5<br><b>Cooldown:</b> 6 || - 
|-
| 40 || Poison Blade 4 || Jab a Potent Poisoned Knife into your target, dealing instant Poison damage. ||  || <b>Damage: </b>125 Poison || <b>Power Cost:</b> 40<br><b>Range:</b> 5<br><b>Cooldown:</b> 6 || - 
|-
| 41 || Wolfsbane Blade 2 || Jab a Potent Wolfsbaned Knife into your target, dealing instant Poison damage, and additional damage to wolves and lycanthropes. || Deals +120 damage over 10 seconds to wolfsbane-vulnerable targets || <b>Damage: </b>90 Poison || <b>Power Cost:</b> 40<br><b>Range:</b> 5<br><b>Cooldown:</b> 6 || - 
|-
| 50 || Wolfsbane Blade 3 || Jab a Heinous Wolfsbaned Knife into your target, dealing instant Poison damage, and additional damage to wolves and lycanthropes. || Deals +150 damage over 10 seconds to wolfsbane-vulnerable targets || <b>Damage: </b>150 Poison || <b>Power Cost:</b> 44<br><b>Range:</b> 5<br><b>Cooldown:</b> 6 || - 
|-
| 50 || Poison Blade 5 || Jab a Deadly Poisoned Knife into your target, dealing instant Poison damage. ||  || <b>Damage: </b>210 Poison || <b>Power Cost:</b> 44<br><b>Range:</b> 5<br><b>Cooldown:</b> 6 || - 
|}
<!-- This is the end of the auto generated content -->
