{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Bonus !! Skill
|-
| 01 || MAX_HEALTH || 5
|-
| 01 || MAX_RAGE || 4
|-
| 01 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 02 || MAX_HEALTH || 6
|-
| 02 || MAX_RAGE || 4
|-
| 02 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 03 || MAX_HEALTH || 6
|-
| 03 || MAX_RAGE || 4
|-
| 03 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 04 || MAX_HEALTH || 6
|-
| 04 || MAX_RAGE || 4
|-
| 04 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 05 || MAX_HEALTH || 12
|-
| 05 || MAX_RAGE || 8
|-
| 05 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 06 || MAX_HEALTH || 6
|-
| 06 || MAX_RAGE || 4
|-
| 06 || MONSTER_COMBAT_XP_VALUE || 6
|-
| 07 || MAX_HEALTH || 6
|-
| 07 || MAX_RAGE || 4
|-
| 07 || MONSTER_COMBAT_XP_VALUE || 7
|-
| 08 || MAX_HEALTH || 6
|-
| 08 || MAX_RAGE || 4
|-
| 08 || MONSTER_COMBAT_XP_VALUE || 7
|-
| 09 || MAX_HEALTH || 6
|-
| 09 || MAX_RAGE || 4
|-
| 09 || MONSTER_COMBAT_XP_VALUE || 7
|-
| 10 || MAX_HEALTH || 12
|-
| 10 || MAX_RAGE || 8
|-
| 10 || MONSTER_COMBAT_XP_VALUE || 7
|-
| 100 || MAX_HEALTH || 120
|-
| 100 || MAX_RAGE || 48
|-
| 100 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 11 || MAX_HEALTH || 6
|-
| 11 || MAX_RAGE || 4
|-
| 11 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 12 || MAX_HEALTH || 6
|-
| 12 || MAX_RAGE || 4
|-
| 12 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 13 || MAX_HEALTH || 6
|-
| 13 || MAX_RAGE || 4
|-
| 13 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 14 || MAX_HEALTH || 6
|-
| 14 || MAX_RAGE || 4
|-
| 14 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 15 || MAX_HEALTH || 12
|-
| 15 || MAX_RAGE || 8
|-
| 15 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 16 || MAX_HEALTH || 6
|-
| 16 || MAX_RAGE || 4
|-
| 16 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 17 || MAX_HEALTH || 6
|-
| 17 || MAX_RAGE || 4
|-
| 17 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 18 || MAX_HEALTH || 6
|-
| 18 || MAX_RAGE || 4
|-
| 18 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 19 || MAX_HEALTH || 6
|-
| 19 || MAX_RAGE || 4
|-
| 19 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 20 || MAX_HEALTH || 12
|-
| 20 || MAX_RAGE || 8
|-
| 20 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 21 || MAX_HEALTH || 9
|-
| 21 || MAX_RAGE || 6
|-
| 21 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 22 || MAX_HEALTH || 9
|-
| 22 || MAX_RAGE || 6
|-
| 22 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 23 || MAX_HEALTH || 9
|-
| 23 || MAX_RAGE || 6
|-
| 23 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 24 || MAX_HEALTH || 9
|-
| 24 || MAX_RAGE || 6
|-
| 24 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 25 || MAX_HEALTH || 18
|-
| 25 || MAX_RAGE || 12
|-
| 25 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 26 || MAX_HEALTH || 9
|-
| 26 || MAX_RAGE || 6
|-
| 26 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 27 || MAX_HEALTH || 9
|-
| 27 || MAX_RAGE || 6
|-
| 27 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 28 || MAX_HEALTH || 9
|-
| 28 || MAX_RAGE || 6
|-
| 28 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 29 || MAX_HEALTH || 9
|-
| 29 || MAX_RAGE || 6
|-
| 29 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 30 || MAX_HEALTH || 18
|-
| 30 || MAX_RAGE || 12
|-
| 30 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 31 || MAX_HEALTH || 9
|-
| 31 || MAX_RAGE || 6
|-
| 31 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 32 || MAX_HEALTH || 9
|-
| 32 || MAX_RAGE || 6
|-
| 32 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 33 || MAX_HEALTH || 9
|-
| 33 || MAX_RAGE || 6
|-
| 33 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 34 || MAX_HEALTH || 9
|-
| 34 || MAX_RAGE || 6
|-
| 34 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 35 || MAX_HEALTH || 18
|-
| 35 || MAX_RAGE || 12
|-
| 35 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 36 || MAX_HEALTH || 12
|-
| 36 || MAX_RAGE || 8
|-
| 36 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 37 || MAX_HEALTH || 12
|-
| 37 || MAX_RAGE || 8
|-
| 37 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 38 || MAX_HEALTH || 12
|-
| 38 || MAX_RAGE || 8
|-
| 38 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 39 || MAX_HEALTH || 12
|-
| 39 || MAX_RAGE || 8
|-
| 39 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 40 || MAX_HEALTH || 24
|-
| 40 || MAX_RAGE || 16
|-
| 40 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 41 || MAX_HEALTH || 18
|-
| 41 || MAX_RAGE || 12
|-
| 41 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 42 || MAX_HEALTH || 18
|-
| 42 || MAX_RAGE || 12
|-
| 42 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 43 || MAX_HEALTH || 18
|-
| 43 || MAX_RAGE || 12
|-
| 43 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 44 || MAX_HEALTH || 18
|-
| 44 || MAX_RAGE || 12
|-
| 44 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 45 || MAX_HEALTH || 30
|-
| 45 || MAX_RAGE || 20
|-
| 45 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 46 || MAX_HEALTH || 24
|-
| 46 || MAX_RAGE || 16
|-
| 46 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 47 || MAX_HEALTH || 24
|-
| 47 || MAX_RAGE || 16
|-
| 47 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 48 || MAX_HEALTH || 24
|-
| 48 || MAX_RAGE || 16
|-
| 48 || MONSTER_COMBAT_XP_VALUE || 3
|-
| 49 || MAX_HEALTH || 24
|-
| 49 || MAX_RAGE || 16
|-
| 49 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 50 || MAX_HEALTH || 24
|-
| 50 || MAX_RAGE || 16
|-
| 50 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 51 || MAX_HEALTH || 30
|-
| 51 || MAX_RAGE || 12
|-
| 51 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 52 || MAX_HEALTH || 30
|-
| 52 || MAX_RAGE || 12
|-
| 52 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 53 || MAX_HEALTH || 30
|-
| 53 || MAX_RAGE || 12
|-
| 53 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 54 || MAX_HEALTH || 30
|-
| 54 || MAX_RAGE || 12
|-
| 54 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 55 || MAX_HEALTH || 60
|-
| 55 || MAX_RAGE || 24
|-
| 55 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 56 || MAX_HEALTH || 30
|-
| 56 || MAX_RAGE || 12
|-
| 56 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 57 || MAX_HEALTH || 30
|-
| 57 || MAX_RAGE || 12
|-
| 57 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 58 || MAX_HEALTH || 30
|-
| 58 || MAX_RAGE || 12
|-
| 58 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 59 || MAX_HEALTH || 30
|-
| 59 || MAX_RAGE || 12
|-
| 59 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 60 || MAX_HEALTH || 60
|-
| 60 || MAX_RAGE || 24
|-
| 60 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 61 || MAX_HEALTH || 30
|-
| 61 || MAX_RAGE || 12
|-
| 61 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 62 || MAX_HEALTH || 30
|-
| 62 || MAX_RAGE || 12
|-
| 62 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 63 || MAX_HEALTH || 30
|-
| 63 || MAX_RAGE || 12
|-
| 63 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 64 || MAX_HEALTH || 30
|-
| 64 || MAX_RAGE || 12
|-
| 64 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 65 || MAX_HEALTH || 60
|-
| 65 || MAX_RAGE || 24
|-
| 65 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 66 || MAX_HEALTH || 30
|-
| 66 || MAX_RAGE || 12
|-
| 66 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 67 || MAX_HEALTH || 30
|-
| 67 || MAX_RAGE || 12
|-
| 67 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 68 || MAX_HEALTH || 30
|-
| 68 || MAX_RAGE || 12
|-
| 68 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 69 || MAX_HEALTH || 30
|-
| 69 || MAX_RAGE || 12
|-
| 69 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 70 || MAX_HEALTH || 60
|-
| 70 || MAX_RAGE || 24
|-
| 70 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 71 || MAX_HEALTH || 45
|-
| 71 || MAX_RAGE || 18
|-
| 71 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 72 || MAX_HEALTH || 45
|-
| 72 || MAX_RAGE || 18
|-
| 72 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 73 || MAX_HEALTH || 45
|-
| 73 || MAX_RAGE || 18
|-
| 73 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 74 || MAX_HEALTH || 45
|-
| 74 || MAX_RAGE || 18
|-
| 74 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 75 || MAX_HEALTH || 90
|-
| 75 || MAX_RAGE || 36
|-
| 75 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 76 || MAX_HEALTH || 45
|-
| 76 || MAX_RAGE || 18
|-
| 76 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 77 || MAX_HEALTH || 45
|-
| 77 || MAX_RAGE || 18
|-
| 77 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 78 || MAX_HEALTH || 45
|-
| 78 || MAX_RAGE || 18
|-
| 78 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 79 || MAX_HEALTH || 45
|-
| 79 || MAX_RAGE || 18
|-
| 79 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 80 || MAX_HEALTH || 90
|-
| 80 || MAX_RAGE || 36
|-
| 80 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 81 || MAX_HEALTH || 45
|-
| 81 || MAX_RAGE || 18
|-
| 81 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 82 || MAX_HEALTH || 45
|-
| 82 || MAX_RAGE || 18
|-
| 82 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 83 || MAX_HEALTH || 45
|-
| 83 || MAX_RAGE || 18
|-
| 83 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 84 || MAX_HEALTH || 45
|-
| 84 || MAX_RAGE || 18
|-
| 84 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 85 || MAX_HEALTH || 90
|-
| 85 || MAX_RAGE || 36
|-
| 85 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 86 || MAX_HEALTH || 60
|-
| 86 || MAX_RAGE || 24
|-
| 86 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 87 || MAX_HEALTH || 60
|-
| 87 || MAX_RAGE || 24
|-
| 87 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 88 || MAX_HEALTH || 60
|-
| 88 || MAX_RAGE || 24
|-
| 88 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 89 || MAX_HEALTH || 60
|-
| 89 || MAX_RAGE || 24
|-
| 89 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 90 || MAX_HEALTH || 120
|-
| 90 || MAX_RAGE || 48
|-
| 90 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 91 || MAX_HEALTH || 90
|-
| 91 || MAX_RAGE || 36
|-
| 91 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 92 || MAX_HEALTH || 90
|-
| 92 || MAX_RAGE || 36
|-
| 92 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 93 || MAX_HEALTH || 90
|-
| 93 || MAX_RAGE || 36
|-
| 93 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 94 || MAX_HEALTH || 90
|-
| 94 || MAX_RAGE || 36
|-
| 94 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 95 || MAX_HEALTH || 150
|-
| 95 || MAX_RAGE || 60
|-
| 95 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 96 || MAX_HEALTH || 120
|-
| 96 || MAX_RAGE || 48
|-
| 96 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 97 || MAX_HEALTH || 120
|-
| 97 || MAX_RAGE || 48
|-
| 97 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 98 || MAX_HEALTH || 120
|-
| 98 || MAX_RAGE || 48
|-
| 98 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 99 || MAX_HEALTH || 120
|-
| 99 || MAX_RAGE || 48
|-
| 99 || MONSTER_COMBAT_XP_VALUE || 2
|}
