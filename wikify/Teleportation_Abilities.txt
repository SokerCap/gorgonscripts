{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Name !! Description !! Special !! style="width: 11%; text-align: center;" | Damage !! style="width: 11%; text-align: center;" | Stats !! Gear Required !! style="width: 5%; text-align: center;" |
|-
| 25 || Organ Displacement || Teleport the target's innards to another location far away. Requires an Aquamarine gem, which is usually consumed, but sometimes ends up inside the monster. ||  ||  || <b>Power Cost:</b> 50<br><b>Range:</b> 30<br><b>Cooldown:</b> 90 || - 
|}
<!-- This is the end of the auto generated content -->
