{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Name !! Description !! Special !! style="width: 11%; text-align: center;" | Damage !! style="width: 11%; text-align: center;" | Stats !! Gear Required !! style="width: 5%; text-align: center;" |
|-
| 2 || Dig Deep || Summon your body's last reserves of Power in an emergency. || You recover 40 Power. ||  || <b>Power Cost:</b> 0<br><b>Range:</b> 4<br><b>Cooldown:</b> 120 || - 
|-
| 10 || Push Onward || Take advantage of your preparedness and training to stay alive longer... at the cost of your armor. || You lose up to 50 Armor and regain that much Health. ||  || <b>Power Cost:</b> 8<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || - 
|-
| 17 || Dig Deep 2 || Summon your body's last reserves of Power in an emergency. || You recover 60 Power. ||  || <b>Power Cost:</b> 0<br><b>Range:</b> 4<br><b>Cooldown:</b> 120 || - 
|-
| 20 || Push Onward 2 || Take advantage of your preparedness and training to stay alive longer... at the cost of your armor. || You lose up to 80 Armor and regain that much Health. ||  || <b>Power Cost:</b> 9<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || - 
|-
| 25 || Dig Deep 3 || Summon your body's last reserves of Power in an emergency. || You recover 80 Power. ||  || <b>Power Cost:</b> 0<br><b>Range:</b> 4<br><b>Cooldown:</b> 120 || - 
|-
| 30 || Push Onward 3 || Take advantage of your preparedness and training to stay alive longer... at the cost of your armor. || You lose up to 130 Armor and regain that much Health. ||  || <b>Power Cost:</b> 11<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || - 
|-
| 38 || Dig Deep 4 || Summon your body's last reserves of Power in an emergency. || You recover 100 Power. ||  || <b>Power Cost:</b> 0<br><b>Range:</b> 4<br><b>Cooldown:</b> 120 || - 
|-
| 40 || Push Onward 4 || Take advantage of your preparedness and training to stay alive longer... at the cost of your armor. || You lose up to 160 Armor and regain that much Health. ||  || <b>Power Cost:</b> 12<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || - 
|-
| 50 || Push Onward 5 || Take advantage of your preparedness and training to stay alive longer... at the cost of your armor. || You lose up to 200 Armor and regain that much Health. ||  || <b>Power Cost:</b> 13<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || - 
|-
| 50 || Dig Deep 5 || Summon your body's last reserves of Power in an emergency. || You recover 120 Power. ||  || <b>Power Cost:</b> 0<br><b>Range:</b> 4<br><b>Cooldown:</b> 120 || - 
|-
| 60 || Dig Deep 6 || Summon your body's last reserves of Power in an emergency. || You recover 140 Power. ||  || <b>Power Cost:</b> 0<br><b>Range:</b> 4<br><b>Cooldown:</b> 120 || - 
|-
| 60 || Push Onward 6 || Take advantage of your preparedness and training to stay alive longer... at the cost of your armor. || You lose up to 250 Armor and regain that much Health. ||  || <b>Power Cost:</b> 15<br><b>Range:</b> 4<br><b>Cooldown:</b> 30 || - 
|}
<!-- This is the end of the auto generated content -->
