{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Bonus !! Skill
|-
| 01 || VULN_COLD || -0.25
|-
| 01 || VULN_ELECTRICITY || -0.25
|-
| 01 || VULN_NATURE || -0.25
|-
| 01 || VULN_ACID || 0.25
|-
| 01 || VULN_FIRE || 0.25
|-
| 01 || VULN_DARKNESS || -1
|}
