{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Bonus !! Skill
|-
| 01 || VULN_ELECTRICITY || -0.5
|-
| 01 || VULN_FIRE || -0.5
|-
| 01 || VULN_PIERCING || 0.25
|-
| 01 || VULN_CRUSHING || 0.25
|-
| 01 || NONCOMBAT_REGEN_HEALTH_MOD || 0.2
|}
