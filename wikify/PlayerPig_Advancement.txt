{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Bonus !! Skill
|-
| 01 || MAX_HEALTH || 27
|-
| 01 || MAX_POWER || 2
|-
| 02 || MAX_HEALTH || 2
|-
| 02 || MAX_POWER || 1
|-
| 03 || MAX_HEALTH || 2
|-
| 03 || MAX_POWER || 2
|-
| 04 || MAX_HEALTH || 2
|-
| 04 || MAX_POWER || 1
|-
| 05 || MAX_HEALTH || 3
|-
| 05 || MAX_POWER || 2
|-
| 06 || MAX_HEALTH || 2
|-
| 06 || MAX_POWER || 2
|-
| 07 || COMBAT_REGEN_HEALTH_DELTA || 1
|-
| 07 || MAX_HEALTH || 2
|-
| 07 || MAX_POWER || 2
|-
| 08 || MAX_HEALTH || 2
|-
| 08 || MAX_POWER || 1
|-
| 09 || MAX_HEALTH || 2
|-
| 09 || MAX_POWER || 1
|-
| 10 || MAX_HEALTH || 3
|-
| 10 || MAX_POWER || 2
|-
| 10 || MITIGATION_NATURE || 1
|-
| 11 || MAX_HEALTH || 2
|-
| 11 || MAX_POWER || 2
|-
| 12 || MAX_HEALTH || 2
|-
| 12 || MAX_POWER || 1
|-
| 13 || MAX_HEALTH || 2
|-
| 13 || MAX_POWER || 2
|-
| 14 || COMBAT_REGEN_HEALTH_DELTA || 1
|-
| 14 || MAX_HEALTH || 2
|-
| 14 || MAX_POWER || 1
|-
| 15 || MAX_HEALTH || 3
|-
| 15 || MAX_POWER || 2
|-
| 16 || MAX_HEALTH || 2
|-
| 16 || MAX_POWER || 2
|-
| 17 || MAX_HEALTH || 2
|-
| 17 || MAX_POWER || 2
|-
| 18 || MAX_HEALTH || 2
|-
| 18 || MAX_POWER || 1
|-
| 19 || MAX_HEALTH || 2
|-
| 19 || MAX_POWER || 1
|-
| 20 || MAX_HEALTH || 3
|-
| 20 || MAX_POWER || 2
|-
| 20 || MITIGATION_NATURE || 1
|-
| 21 || COMBAT_REGEN_HEALTH_DELTA || 1
|-
| 21 || MAX_HEALTH || 2
|-
| 21 || MAX_POWER || 2
|-
| 22 || MAX_HEALTH || 2
|-
| 22 || MAX_POWER || 1
|-
| 23 || MAX_HEALTH || 2
|-
| 23 || MAX_POWER || 2
|-
| 24 || MAX_HEALTH || 2
|-
| 24 || MAX_POWER || 1
|-
| 25 || MAX_HEALTH || 3
|-
| 25 || MAX_POWER || 2
|-
| 26 || MAX_HEALTH || 2
|-
| 26 || MAX_POWER || 2
|-
| 27 || MAX_HEALTH || 2
|-
| 27 || MAX_POWER || 2
|-
| 28 || COMBAT_REGEN_HEALTH_DELTA || 1
|-
| 28 || MAX_HEALTH || 2
|-
| 28 || MAX_POWER || 1
|-
| 29 || MAX_HEALTH || 2
|-
| 29 || MAX_POWER || 1
|-
| 30 || MAX_HEALTH || 3
|-
| 30 || MAX_POWER || 2
|-
| 30 || MITIGATION_NATURE || 1
|-
| 31 || MAX_HEALTH || 2
|-
| 31 || MAX_POWER || 2
|-
| 32 || MAX_HEALTH || 2
|-
| 32 || MAX_POWER || 1
|-
| 33 || MAX_HEALTH || 2
|-
| 33 || MAX_POWER || 2
|-
| 34 || MAX_HEALTH || 2
|-
| 34 || MAX_POWER || 1
|-
| 35 || COMBAT_REGEN_HEALTH_DELTA || 1
|-
| 35 || MAX_HEALTH || 3
|-
| 35 || MAX_POWER || 2
|-
| 36 || MAX_HEALTH || 2
|-
| 36 || MAX_POWER || 2
|-
| 37 || MAX_HEALTH || 2
|-
| 37 || MAX_POWER || 2
|-
| 38 || MAX_HEALTH || 2
|-
| 38 || MAX_POWER || 1
|-
| 39 || MAX_HEALTH || 2
|-
| 39 || MAX_POWER || 1
|-
| 40 || MAX_HEALTH || 3
|-
| 40 || MAX_POWER || 2
|-
| 40 || MITIGATION_NATURE || 1
|-
| 41 || MAX_HEALTH || 2
|-
| 41 || MAX_POWER || 2
|-
| 42 || COMBAT_REGEN_HEALTH_DELTA || 1
|-
| 42 || MAX_HEALTH || 2
|-
| 42 || MAX_POWER || 1
|-
| 43 || MAX_HEALTH || 2
|-
| 43 || MAX_POWER || 2
|-
| 44 || MAX_HEALTH || 2
|-
| 44 || MAX_POWER || 1
|-
| 45 || MAX_HEALTH || 3
|-
| 45 || MAX_POWER || 2
|-
| 46 || MAX_HEALTH || 2
|-
| 46 || MAX_POWER || 2
|-
| 47 || MAX_HEALTH || 2
|-
| 47 || MAX_POWER || 2
|-
| 48 || MAX_HEALTH || 2
|-
| 48 || MAX_POWER || 1
|-
| 49 || COMBAT_REGEN_HEALTH_DELTA || 1
|-
| 49 || MAX_HEALTH || 2
|-
| 49 || MAX_POWER || 1
|-
| 50 || MAX_HEALTH || 3
|-
| 50 || MAX_POWER || 2
|-
| 50 || MITIGATION_NATURE || 1
|}
