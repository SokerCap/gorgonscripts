{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Bonus !! Skill
|-
| 01 || MAX_ARMOR || 2
|-
| 01 || MAX_RAGE || 1
|-
| 01 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 02 || MAX_ARMOR || 3
|-
| 02 || MAX_RAGE || 1
|-
| 02 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 03 || MAX_ARMOR || 3
|-
| 03 || MAX_RAGE || 1
|-
| 03 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 04 || MAX_ARMOR || 3
|-
| 04 || MAX_RAGE || 1
|-
| 04 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 05 || MAX_ARMOR || 6
|-
| 05 || MAX_RAGE || 3
|-
| 05 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 06 || MAX_ARMOR || 3
|-
| 06 || MAX_RAGE || 1
|-
| 06 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 07 || MAX_ARMOR || 3
|-
| 07 || MAX_RAGE || 1
|-
| 07 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 08 || MAX_ARMOR || 3
|-
| 08 || MAX_RAGE || 1
|-
| 08 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 09 || MAX_ARMOR || 3
|-
| 09 || MAX_RAGE || 1
|-
| 09 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 10 || MAX_ARMOR || 6
|-
| 10 || MAX_RAGE || 3
|-
| 10 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 100 || MAX_ARMOR || 34
|-
| 100 || MAX_RAGE || 8
|-
| 100 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 11 || MAX_ARMOR || 3
|-
| 11 || MAX_RAGE || 1
|-
| 11 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 12 || MAX_ARMOR || 3
|-
| 12 || MAX_RAGE || 1
|-
| 12 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 13 || MAX_ARMOR || 3
|-
| 13 || MAX_RAGE || 1
|-
| 13 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 14 || MAX_ARMOR || 3
|-
| 14 || MAX_RAGE || 1
|-
| 14 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 15 || MAX_ARMOR || 6
|-
| 15 || MAX_RAGE || 3
|-
| 15 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 16 || MAX_ARMOR || 3
|-
| 16 || MAX_RAGE || 1
|-
| 16 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 17 || MAX_ARMOR || 3
|-
| 17 || MAX_RAGE || 1
|-
| 17 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 18 || MAX_ARMOR || 3
|-
| 18 || MAX_RAGE || 1
|-
| 18 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 19 || MAX_ARMOR || 3
|-
| 19 || MAX_RAGE || 1
|-
| 19 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 20 || MAX_ARMOR || 6
|-
| 20 || MAX_RAGE || 3
|-
| 20 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 21 || MAX_ARMOR || 4
|-
| 21 || MAX_RAGE || 2
|-
| 21 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 22 || MAX_ARMOR || 4
|-
| 22 || MAX_RAGE || 2
|-
| 22 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 23 || MAX_ARMOR || 4
|-
| 23 || MAX_RAGE || 2
|-
| 23 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 24 || MAX_ARMOR || 4
|-
| 24 || MAX_RAGE || 2
|-
| 24 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 25 || MAX_ARMOR || 8
|-
| 25 || MAX_RAGE || 4
|-
| 25 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 26 || MAX_ARMOR || 4
|-
| 26 || MAX_RAGE || 2
|-
| 26 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 27 || MAX_ARMOR || 4
|-
| 27 || MAX_RAGE || 2
|-
| 27 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 28 || MAX_ARMOR || 4
|-
| 28 || MAX_RAGE || 2
|-
| 28 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 29 || MAX_ARMOR || 4
|-
| 29 || MAX_RAGE || 2
|-
| 29 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 30 || MAX_ARMOR || 8
|-
| 30 || MAX_RAGE || 4
|-
| 30 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 31 || MAX_ARMOR || 4
|-
| 31 || MAX_RAGE || 2
|-
| 31 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 32 || MAX_ARMOR || 4
|-
| 32 || MAX_RAGE || 2
|-
| 32 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 33 || MAX_ARMOR || 4
|-
| 33 || MAX_RAGE || 2
|-
| 33 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 34 || MAX_ARMOR || 4
|-
| 34 || MAX_RAGE || 2
|-
| 34 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 35 || MAX_ARMOR || 8
|-
| 35 || MAX_RAGE || 4
|-
| 35 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 36 || MAX_ARMOR || 6
|-
| 36 || MAX_RAGE || 3
|-
| 36 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 37 || MAX_ARMOR || 6
|-
| 37 || MAX_RAGE || 3
|-
| 37 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 38 || MAX_ARMOR || 6
|-
| 38 || MAX_RAGE || 3
|-
| 38 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 39 || MAX_ARMOR || 6
|-
| 39 || MAX_RAGE || 3
|-
| 39 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 40 || MAX_ARMOR || 11
|-
| 40 || MAX_RAGE || 5
|-
| 40 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 41 || MAX_ARMOR || 8
|-
| 41 || MAX_RAGE || 4
|-
| 41 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 42 || MAX_ARMOR || 8
|-
| 42 || MAX_RAGE || 4
|-
| 42 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 43 || MAX_ARMOR || 8
|-
| 43 || MAX_RAGE || 4
|-
| 43 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 44 || MAX_ARMOR || 8
|-
| 44 || MAX_RAGE || 4
|-
| 44 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 45 || MAX_ARMOR || 14
|-
| 45 || MAX_RAGE || 7
|-
| 45 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 46 || MAX_ARMOR || 11
|-
| 46 || MAX_RAGE || 5
|-
| 46 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 47 || MAX_ARMOR || 11
|-
| 47 || MAX_RAGE || 5
|-
| 47 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 48 || MAX_ARMOR || 11
|-
| 48 || MAX_RAGE || 5
|-
| 48 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 49 || MAX_ARMOR || 11
|-
| 49 || MAX_RAGE || 5
|-
| 49 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 50 || MAX_ARMOR || 11
|-
| 50 || MAX_RAGE || 5
|-
| 50 || MONSTER_COMBAT_XP_VALUE || 1
|-
| 51 || MAX_ARMOR || 8
|-
| 51 || MAX_RAGE || 2
|-
| 51 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 52 || MAX_ARMOR || 8
|-
| 52 || MAX_RAGE || 2
|-
| 52 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 53 || MAX_ARMOR || 8
|-
| 53 || MAX_RAGE || 2
|-
| 53 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 54 || MAX_ARMOR || 8
|-
| 54 || MAX_RAGE || 2
|-
| 54 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 55 || MAX_ARMOR || 17
|-
| 55 || MAX_RAGE || 4
|-
| 55 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 56 || MAX_ARMOR || 8
|-
| 56 || MAX_RAGE || 2
|-
| 56 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 57 || MAX_ARMOR || 8
|-
| 57 || MAX_RAGE || 2
|-
| 57 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 58 || MAX_ARMOR || 8
|-
| 58 || MAX_RAGE || 2
|-
| 58 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 59 || MAX_ARMOR || 8
|-
| 59 || MAX_RAGE || 2
|-
| 59 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 60 || MAX_ARMOR || 17
|-
| 60 || MAX_RAGE || 4
|-
| 60 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 61 || MAX_ARMOR || 8
|-
| 61 || MAX_RAGE || 2
|-
| 61 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 62 || MAX_ARMOR || 8
|-
| 62 || MAX_RAGE || 2
|-
| 62 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 63 || MAX_ARMOR || 8
|-
| 63 || MAX_RAGE || 2
|-
| 63 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 64 || MAX_ARMOR || 8
|-
| 64 || MAX_RAGE || 2
|-
| 64 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 65 || MAX_ARMOR || 17
|-
| 65 || MAX_RAGE || 4
|-
| 65 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 66 || MAX_ARMOR || 8
|-
| 66 || MAX_RAGE || 2
|-
| 66 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 67 || MAX_ARMOR || 8
|-
| 67 || MAX_RAGE || 2
|-
| 67 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 68 || MAX_ARMOR || 8
|-
| 68 || MAX_RAGE || 2
|-
| 68 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 69 || MAX_ARMOR || 8
|-
| 69 || MAX_RAGE || 2
|-
| 69 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 70 || MAX_ARMOR || 17
|-
| 70 || MAX_RAGE || 4
|-
| 70 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 71 || MAX_ARMOR || 13
|-
| 71 || MAX_RAGE || 3
|-
| 71 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 72 || MAX_ARMOR || 13
|-
| 72 || MAX_RAGE || 3
|-
| 72 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 73 || MAX_ARMOR || 13
|-
| 73 || MAX_RAGE || 3
|-
| 73 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 74 || MAX_ARMOR || 13
|-
| 74 || MAX_RAGE || 3
|-
| 74 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 75 || MAX_ARMOR || 25
|-
| 75 || MAX_RAGE || 6
|-
| 75 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 76 || MAX_ARMOR || 13
|-
| 76 || MAX_RAGE || 3
|-
| 76 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 77 || MAX_ARMOR || 13
|-
| 77 || MAX_RAGE || 3
|-
| 77 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 78 || MAX_ARMOR || 13
|-
| 78 || MAX_RAGE || 3
|-
| 78 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 79 || MAX_ARMOR || 13
|-
| 79 || MAX_RAGE || 3
|-
| 79 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 80 || MAX_ARMOR || 25
|-
| 80 || MAX_RAGE || 6
|-
| 80 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 81 || MAX_ARMOR || 13
|-
| 81 || MAX_RAGE || 3
|-
| 81 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 82 || MAX_ARMOR || 13
|-
| 82 || MAX_RAGE || 3
|-
| 82 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 83 || MAX_ARMOR || 13
|-
| 83 || MAX_RAGE || 3
|-
| 83 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 84 || MAX_ARMOR || 13
|-
| 84 || MAX_RAGE || 3
|-
| 84 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 85 || MAX_ARMOR || 25
|-
| 85 || MAX_RAGE || 6
|-
| 85 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 86 || MAX_ARMOR || 17
|-
| 86 || MAX_RAGE || 4
|-
| 86 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 87 || MAX_ARMOR || 17
|-
| 87 || MAX_RAGE || 4
|-
| 87 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 88 || MAX_ARMOR || 17
|-
| 88 || MAX_RAGE || 4
|-
| 88 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 89 || MAX_ARMOR || 17
|-
| 89 || MAX_RAGE || 4
|-
| 89 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 90 || MAX_ARMOR || 34
|-
| 90 || MAX_RAGE || 8
|-
| 90 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 91 || MAX_ARMOR || 25
|-
| 91 || MAX_RAGE || 6
|-
| 91 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 92 || MAX_ARMOR || 25
|-
| 92 || MAX_RAGE || 6
|-
| 92 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 93 || MAX_ARMOR || 25
|-
| 93 || MAX_RAGE || 6
|-
| 93 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 94 || MAX_ARMOR || 25
|-
| 94 || MAX_RAGE || 6
|-
| 94 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 95 || MAX_ARMOR || 42
|-
| 95 || MAX_RAGE || 10
|-
| 95 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 96 || MAX_ARMOR || 34
|-
| 96 || MAX_RAGE || 8
|-
| 96 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 97 || MAX_ARMOR || 34
|-
| 97 || MAX_RAGE || 8
|-
| 97 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 98 || MAX_ARMOR || 34
|-
| 98 || MAX_RAGE || 8
|-
| 98 || MONSTER_COMBAT_XP_VALUE || 2
|-
| 99 || MAX_ARMOR || 34
|-
| 99 || MAX_RAGE || 8
|-
| 99 || MONSTER_COMBAT_XP_VALUE || 2
|}
