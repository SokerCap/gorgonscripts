{| {{STDT|sortable}} class='table mw-collapsible mw-collapsed' style='width:90%;'
! Level !! Bonus !! Skill
|-
| 01 || LOOT_BOOST_CHANCE_UNCOMMON || 1
|-
| 01 || LOOT_BOOST_CHANCE_RARE || 0.45
|-
| 01 || LOOT_BOOST_CHANCE_EXCEPTIONAL || 0.15
|-
| 01 || LOOT_BOOST_CHANCE_EPIC || 0.1
|}
